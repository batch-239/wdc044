package com.zuitt.wdc044.models;
// import this
import javax.persistence.*;
import javax.persistence.Entity;

@Entity
// mark this Java object as representation of a database table via Entity
@Table(name = "posts")
//designate the table name
// We need to specify its plural form
public class Post {
    @Id
    // indicates that this property is the primary key
    @GeneratedValue
    // auto-increments the ID.
    private Long id;

    // class properties that represent a table column
    @Column
    private String title;

    @Column
    private String content;

    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    // default constructor, this is needed when retrieving posts
    public Post(){}

    public  Post(String title, String content){
        this.title = title;
        this.content = content;
    }

    //getter and setter
    public String getTitle(){
        return title;
    }

    public void setTitle(String title){
        this.title = title;
    }

    public  String getContent(){
        return content;
    }

    public void setContent(String content){
        this.content = content;
    }

    public User getUser(){
        return user;
    }

    public void setUser(User user){
        this.user = user;
    }
}
