package com.zuitt.wdc044.services;

import com.zuitt.wdc044.models.User;
import com.zuitt.wdc044.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

// we are using implements because the UserService.java is an interface, while extends is used for class
@Service
// This is used to indicate that it holds the actual business logic
public class UserServiceImpl implements UserService {
    @Autowired
    // This is used to access objects and methods of the another class, Without instantiating
    private UserRepository userRepository;

    // create a user
    public void createUser(User user){
        userRepository.save(user);
    }

    // check if ser exists
        // return the entity based on the given criteria
        // an empty instance of the optional class.
    public Optional<User>findByUsername(String username){
      return Optional.ofNullable(userRepository.findByUsername(username));
    };

}
