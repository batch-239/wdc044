package com.zuitt.wdc044;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


// "@SpringBootApplication" this is called as "Annotations" mark.
// Annotations are used to provide supplemental information about the program.
// These are used to manage and configure the behavior of the framework.
// Spring Boot scans for classes in its class path upon running and assigns behaviors on them based on their annotations.
// To manage and configure behavior of a framework

// To Specify the main class of the Springboot application.
@SpringBootApplication
@RestController
// Tells the springboot that this will handle endpoints for web	request.
// it is the API
public class Wdc044Application {

	public static void main(String[] args) {

		SpringApplication.run(Wdc044Application.class, args);
	}
	// This is used for mapping HTTP GET request.
	@GetMapping("/hello")
//don't use ";" because the next line will follow
		public String hello(@RequestParam (value = "name", defaultValue = "World") String name){
			//To get the specific value in the URL
			// "@RequestParam" is used to extract query parameters
			//name = john; Hello john.
			// name = World; Hello World.
			// To append the URL with a name parameter we do the following:
				//http:localhost:8080/hello?name=john
			return  String.format("Hello %s", name);
			// "%s format specifier"

		}
	// S05
	@GetMapping("/hi")
	public String hi(@RequestParam (value = "user", defaultValue = "User") String user){
		return  String.format("Hi %s !", user);
	}

	// Stretch goals
	//localhost:8080/nameAge?name=nameVal&age=ageVal
	@GetMapping("/nameAge")
	public String nameAge(@RequestParam(value = "name", defaultValue = "Juan") String name, @RequestParam(value = "age", defaultValue = "18") String age){
		return String.format("Hello %s! Your age is %s.", name, age);
	}

}
